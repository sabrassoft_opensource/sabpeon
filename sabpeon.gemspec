
DESC = <<-EOD
Sabras Peon:
	an incredibly simple task runner library,
	intended for quick & dirty build & test cmd line scripts
	yes, you should probably just learn Rake instead
EOD

Gem::Specification.new do |gem|
	gem.authors = 'Sabras Soft LLC'
	gem.name = 'sabpeon'
	gem.version = '1.0.0'
	gem.date = Date.today.to_s
	gem.summary = 'SabPeon: Simple, Quick, & Dirty build task library'
	gem.description = DESC
	gem.require_path = '.'
	gem.files = Dir['sabpeon.rb']
	gem.license = 'MIT'
end
