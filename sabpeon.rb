#
# Sabras Peon:
#	an incredibly simple task runner library,
#	intended for quick & dirty build & test cmd line scripts
#	yes, you should probably just learn Rake instead
#
#

module SabPeon 

	def self.parseCliArgs!(taskSet, opts = {})
		source = opts[:source]
		source = $* if source.nil?
	
		onlyInputs = opts[:onlyInputs] ? opts[:onlyInputs] : []
		ignoreInputs = opts[:ignoreInputs] ? opts[:ignoreInputs] : []
			
		onlyFlags = opts[:onlyFlags] ? opts[:onlyFlags] : []
		ignoreFlags = opts[:ignoreFlags] ? opts[:ignoreFlags] : []
	
		toKey = lambda {|raw| raw.to_s.to_sym }
	
		check = lambda do |key, only, ignore|
			return false if ignore.include?(key)
			return true if only.empty?
			return only.include?(key)
		end
	
		source.each do |cliArg| case cliArg
			when /^[-]i[:](\w+)[=](.+)$/
				name = toKey.call($1)
				next unless check.call(name, onlyInputs, ignoreInputs)
				taskSet.inputs[name] = $2
			when /^[-]f[:](\w+)$/
				name = toKey.call($1)
				next unless check.call(name, onlyFlags, ignoreFlags)
				taskSet.flags[name] = true
			when /^[-]f[:](\w+)[=](1|0)$/
				name = toKey.call($1)
				next unless check.call(name, onlyFlags, ignoreFlags)
				taskSet.flags[name] = ($2.to_i == 1)
		end end
	end
	
	class Context
		attr_reader :inputs, :flags
	
		def initialize()
			@inputs = {}
			@flags = Hash.new {|fs,k| fs[k] = false}
		end
	
		def inputs=(ins = {})
			@inputs.merge!(ins) unless ins.nil?
			self
		end
		
		def flags=(fs = {})
			@flags.merge!(fs) unless fs.nil?
			self
		end
	
	end
	
	module Contextualized
		
		attr_reader :context
		
		def inputs()
			@context.inputs
		end
		def inputs=(ins = {})
			@context.inputs = ins
			self
		end
		
		def flags()
			@context.flags
		end
		def flags=(fs = {})
			@context.flags = fs
			self
		end
	
	end
	
	class TaskSet
		include Contextualized
		
		def initialize(context = Context.new)
			@context = context
			@taskset = []
			@priority = {}
			@order = {}
			
			@preGo = []
			@postGo = []
			@preTask = []
			@postTask = []
			
			@checkReturn = nil
			@checkInterrupt = nil
		end
	
		def tasks()
			@taskset.dup
		end
		
		def drop(*tids)
			@taskset.delete_if {|t| tids.include?(t.id) }
			@priority.delete_if {|tid,p| tids.include?(tid) }
			@order.delete_if {|tid,o| tids.include?(tid) }
			self
		end
		alias :- :drop
	
		def add(task)
			@order[task.id] = @taskset.empty? ? 0 : @order.values.max+1
			@priority[task.id] = task.priority
			@taskset << task
			self
		end
		alias :<< :add
		
		def task(id, *opts, &body)
			self << Task.new(id, *opts, &body)
			self
		end
		
		def checkInterrupt(&check)
			@checkInterrupt = case check.arity
				when 1 then lambda do |tset, opts, ex| 
					tset.instance_exec(ex, &check)
				end
				when 2 then lambda do |tset, opts, ex| 
					tset.instance_exec(ex, opts, &check)
				end
				else throw "invalid checkInterrupt signature (#{check.arity})"
			end
			self
		end
		
		def checkReturn(&check)
			@checkReturn = case check.arity
				when 1 then lambda do |tset, opts, ret| 
					tset.instance_exec(ret, &check)
				end
				when 2 then lambda do |tset, opts, ret| 
					tset.instance_exec(ret, opts, &check)
				end
				else raise "invalid checkReturn signature (#{check.arity})"
			end
			self
		end
		
		def preGo(&behave)
			defOnGo(@preGo, 'preGo', &behave)
		end
		def postGo(&behave)
			defOnGo(@postGo, 'postGo', &behave)
		end
		def defOnGo(bset, bsetnm, &behave)
			bset << case behave.arity
				when 0 then lambda do |tset, opts|  
					tset.instance_exec(&behave)
				end
				when 1 then lambda do |tset, opts|
					tset.instance_exec(opts, &behave)
				end
				when -1 then lambda do |tset, opts|
					tset.instance_exec(*opts, &behave)
				end
				else raise "invalid #{bsetnm} signature (#{behave.arity})"
			end
			self
		end
		private :defOnGo
		
		def preTask(&behave)
			defOnTask(@preTask, 'preTask', &behave)
		end
		def postTask(&behave)
			defOnTask(@postTask, 'postTask', &behave)
		end
		def defOnTask(bset, bsetnm, &behave)
			bset << case behave.arity
				when 0 then lambda do |tset, t, opts|  
					tset.instance_exec(&behave)
				end
				when 1 then lambda do |tset, t, opts|
					tset.instance_exec(t, &behave)
				end
				when 2 then lambda do |tset, t, opts|
					tset.instance_exec(t, opts, &behave)
				end
				when -1 then lambda do |tset, t, opts|
					tset.instance_exec(*opts, &behave)
				end
				when -2 then lambda do |tset, t, opts|
					tset.instance_exec(t, *opts, &behave)
				end
				else raise "invalid #{bsetnm} signature (#{behave.arity})"
			end
			self
		end
		private :defOnTask
	
		def prioritize(pri, *tids)
			tids.each {|t| @priority[t] = pri }
			self
		end
	
		def go!(*opts)
			@preGo.each {|preGo| preGo.call(self, opts)}
		
			sort = lambda do |a,b| 
				[
					@priority[a.id] <=> @priority[b.id],
					@order[a.id] <=> @order[b.id]
				].each {|check| return check unless check == 0}
				0
			end
		
			@taskset.uniq.sort(&sort).each do |task| begin
				@preTask.each {|preTask| preTask.call(self, task, opts) }
				status = task.execute!(@context)
				@postTask.each {|postTask| postTask.call(self, task, opts) }
				next unless @checkReturn
				check = @checkReturn.call(self, opts, status)
				break unless check
			rescue => e
				raise e unless @checkInterrupt
				check = @checkInterrupt.call(self, opts, e)
				break unless check
			end end
			
			@postGo.each {|postGo| postGo.call(self, opts)}
			
			self
		end
	
	end
	
	class Task 
	
		class Execution
			include Contextualized
			attr_reader :task, :id
			
			class Factory
				attr_reader :executionType
			
				def initialize(ec = Task::Execution)
					@executionType = ec
				end
			
				def build(t, cxt)
					@executionType.new(t, cxt)
				end
			end
			
			def self.factory()
				Task::Execution::Factory.new(self)
			end
			
			def initialize(task, cxt)
				@id = task.id
				@task = task
				@context = cxt
			end
		end
	
		class Guard
			def initialize(&block)
				@block = case (block.nil? ? block.arity : nil)
					when 0 then lambda {|c| block.call()}
					when 1 then block 
					when nil then nil
					else throw "invalid guard (arity = #{block.arity}"
				end
			end
			
			def pass?(cxt)
				@block.nil? ? true : @block.call(cxt)
			end
		end
		
		class FlagGuard < Guard
			def initialize(flag, value = true)
				@v = value
				@f = flag
			end
			
			def pass?(cxt)
				fv = cxt.flags[@f]
				@v ?  fv : !fv
			end
		end
		
		class Meta
			attr_reader :description
			
			def initialize(desc, sundry = {})
				@description = desc
				@sundry = {}
				@sundry.merge!(sundry) if sundry.is_a?(Hash)
			end
			
			def sundry()
				@sundry.dup
			end	
		end
		
		class Priority
			attr_reader :priority
			def initialize(pri)
				@priority = pri
			end
			
			def <=>(other)
				self.priority <=> other.priority
			end
		end
		
		EmptyTaskBody = Proc.new {|cxt| }
		DefaultPriority = Task::Priority.new(0)

		attr_accessor :priority 
		attr_reader :meta , :id

		def initialize(id, *opts, &body)
			@id = id
			@body = body.nil? ? Task::EmptyTaskBody : body
			@meta = Task::Meta.new(id)
			@priority = Task::DefaultPriority
			@execFactory = Task::Execution.factory
			@guards = []
			
			opts.each do |opt| case opt
				when Task::Meta then @meta = opt
				when String then @meta = Task::Meta.new(opt)
				when Task::Guard then @guards << opt
				when TrueClass then @guards << Task::Guard.new { true } 
				when FalseClass then @guards << Task::Guard.new { false }
				when Task::Priority then @priority = opt
				when Task::Execution::Factory then @execFactory = opt
			end end
		end
		
		def executionFactory()
			@execFactory
		end
		
		def executionType()
			@execFactory.executionType
		end
		
		def canExecute?(context)
			@guards.each do |guard|
				return false unless guard.pass?(context)
			end
			true
		end

		def execute!(context)
			if canExecute?(context)
				@execFactory.build(self, context).instance_exec(&@body)
			end
		end

	end

end
