

require 'sabpeon'

class Random
	def bool
		(self.rand(2) + 1).even?
	end
end

TaskSet = SabPeon::TaskSet.new


rand = Random.new
inputs = {}
flags = {}

#randomly populate 10 ea flags & inputs
10.times do |i|
	key = i.to_s.to_sym

	input = ""
	inputLen = rand.rand(5) + 5 
	inputLen.times {|c| input += rand.rand(10).to_s }
	inputs[key] = input

	flags[key] = rand.bool
end

#make sure we have at least 3 true flags
trueFlagCount = 0
flags.each do |fk, v|
	trueFlagCount += 1 if v
end
if trueFlagCount < 3
	flags[ flags.keys[0] ] = true
	flags[ flags.keys[1] ] = true
	flags[ flags.keys[2] ] = true
end



#test providing inputs
puts "INPUTS"
TaskSet.inputs.clear
TaskSet.flags.clear

args = []
checks = []

while checks.size < 5
	inkey = rand.rand(10).to_s.to_sym
	next if checks.include?(inkey)
	checks << inkey
	args << "-i:#{inkey}=#{inputs[inkey]}"
end

SabPeon.parseCliArgs!(TaskSet, :source => args)

count = 0
checks.each do |inkey|
	puts "checking input #{inkey}, e=#{inputs[inkey]} a=#{TaskSet.inputs[inkey]}"
	count += 1 if inputs[inkey] == TaskSet.inputs[inkey]
end
puts (count == checks.size) ? "PASS" : "FAIL"


#test providing short form flags
puts "\nSHORT FORM FLAGS"
TaskSet.inputs.clear
TaskSet.flags.clear

args = []
checks = []

flags.each do |fk, v|
	next unless v
	checks << fk
	args << "-f:#{fk}"
	break if checks.size >= 5
end

SabPeon.parseCliArgs!(TaskSet, :source => args)

count = 0
flags.each do |fk,v|
	puts "checking short form flags: #{fk} e=#{checks.include?(fk)} a=#{TaskSet.flags[fk]}"
	tsValue = TaskSet.flags[fk]
	pass = checks.include?(fk) ? tsValue : !tsValue 
	count += 1 if pass
end
puts (count == flags.size) ? "PASS" : "FAIL"

#test providing long form flags
puts "\nLONG FORM FLAGS"
TaskSet.inputs.clear
TaskSet.flags.clear

args = []
checks = []

while checks.size < 5
	fk = rand.rand(10).to_s.to_sym
	next if checks.include?(fk)
	checks << fk
	args << "-f:#{fk}=#{flags[fk] ? 1 : 0}"
end

SabPeon.parseCliArgs!(TaskSet, :source => args)

count = 0
checks.each do |fk|
	puts "checking long form flag #{fk}, e=#{flags[fk]} a=#{TaskSet.flags[fk]}"
	count += 1 if flags[fk] == TaskSet.flags[fk]
end
puts (count == checks.size) ? "PASS" : "FAIL"
