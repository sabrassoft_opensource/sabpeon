

#ensure can access & change task context as expected

require 'sabpeon'

$taskSet = SabPeon::TaskSet.new
$sampleTask = SabPeon::Task.new(0, "sample")
$rand = Random.new

def printInput(i, ev)
	[
		"i=#{i}",
		"ev=#{ev}",
		"cxt=#{$taskSet.context.inputs[i]}",
		"tst=#{$taskSet.inputs[i]}",
	].join(' ')
end

def checkInput(i, ev)
	check = 0
	[
		$taskSet.context.inputs[i],
		$taskSet.inputs[i],
	].each { |av| check += 1 if av == ev }
	check == 2
end

def printFlag(f, ev)
	[
		"f=#{f}",
		"ev=#{ev}",
		"cxt=#{$taskSet.context.flags[f]}",
		"tst=#{$taskSet.flags[f]}",
	].join(' ')
end

def checkFlag(f, ev)
	check = 0
	[
		$taskSet.context.flags[f],
		$taskSet.flags[f],
	].each { |av| check += 1 if av == ev }
	check == 2
end


inputCount = 10
passCount = 0

inputCount.times do |i|
	puts "\n"

	inputPassCount = 0
	
	$taskSet.context.inputs[i] = i
	$taskSet.context.flags[i] = true
	
	puts "input aliasing read check: " + printInput(i,i)
	inputPassCount += 1 if checkInput(i, i)
	
	puts "flag aliasing read check: " +  printFlag(i, true) 
	inputPassCount += 1 if checkFlag(i, true)
	
	$taskSet.context.inputs[i] += 1
	$taskSet.inputs[i] += 1
	puts "input aliasing write check: " + printInput(i, i+2)
	inputPassCount += 1 if checkInput(i, i+2)
	
	$taskSet.context.flags[i] = false
	puts "flag aliasing write check 1o2: " + printFlag(i, false)
	inputPassCount += 1 if checkFlag(i, false)
	
	$taskSet.flags[i] = true
	puts "flag aliasing write check 2o2: " + printFlag(i, true)
	inputPassCount += 1 if checkFlag(i, true)
	
	passCount += 1 if inputPassCount == 5
end

puts "\nFINAL: #{passCount}/#{inputCount}"
if passCount == inputCount
	puts "\n\nPASS!"
else
	puts "\n\nFAIL!"
end




