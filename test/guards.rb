
#test task guards

require 'sabpeon'

class NegGuard < SabPeon::Task::Guard
	attr_reader :flag

	def initialize(f)
		@flag = f
		throw 'bad!' if f.nil?
	end
	
	def neg?()
		true
	end

	def pass?(cxt)
		f = @flag
		puts "!G: #{f} false #{cxt.flags[f]}"
		!cxt.flags[f]
	end

end

class PosGuard < SabPeon::Task::Guard
	attr_reader :flag
	
	def initialize(f)
		@flag = f
		throw 'bad!' if f.nil?
	end

	def neg?()
		false
	end

	def pass?(cxt)
		f = @flag
		puts "!G: #{f} true #{cxt.flags[f]}"
		cxt.flags[f]
	end

end

class Random
	def bool
		(self.rand(2) + 1).even?
	end
end


#create 3-10 tasks, ea. with 1-5 guards
#execute and check, with 3x rollover on flags

rand = Random.new
taskCount = rand.rand(7) + 3
runCount = 3

$taskSet = SabPeon::TaskSet.new
$taskSet.preTask do |task|
	puts "\n#{task.id}"
end

$testData = {}
$testData[:check] = true
$testData[:gix] = {}
runCount.times do |rc|
	$testData[rc] = Hash.new {|a,i| a[i] = false}
end

def crossCheckGuard(i)
	gixs = $testData[:gix][i]
	c = 0
	
	gixs.each do |gix|
		f = gix.flag
		n = gix.neg?
		v = $taskSet.flags[f]
		c += 1 if n ? !v : v
	end
	
	c == gixs.size
end

flagCount = rand.rand(10) + 10

# generate tasks
puts "creating tasks"
taskCount.times do |i|
	guards = []
	checks = []
	guardCount = rand.rand(4) + 1
	guardedFlags = []
	guardCount.times do |g|
		negated = rand.bool
		flag = rand.rand(flagCount)
		while guardedFlags.include?(flag)
			flag = rand.rand(flagCount) 
		end
		guard = negated ? NegGuard.new(flag) : PosGuard.new(flag)
		guards << guard
		guardedFlags << flag
	end
	
	$testData[:gix][i] = guards
	
	puts "create task #{i}: #{guards.size} guards"
	$taskSet.task(i, *guards) do
#		puts "run T(#{id})"
		runNum = inputs[:runNum]
		$testData[runNum][i] = true
	end
	
end

runCount.times do |runNum|
	$taskSet.inputs[:runNum] = runNum

	#generate flags
	flagCount.times do |f|
		$taskSet.flags[f] = rand.bool
	end

	#execute task set
	puts "\n\nRun Number: #{runNum}"
	$taskSet.go!
	
	#check task set
	puts "\n"
	check = 0
	taskCount.times do |i|
		ex = crossCheckGuard(i)
		ac = $testData[runNum][i]
		puts "#{i} => #{ac} vs #{ex}"
		check += 1 if ac == ex
	end
	puts "\nPassed #{check} Checks"
	$testData[:check] = false unless (check == taskCount) 
end

if $testData[:check]
	puts "\n\nPASS!"
else
	puts "\n\nFAIL!"
end
