
# test basic operation

require 'rubygems'
require 'sabpeon'

#create 3-10 tasks, execute each and check

rand = Random.new

taskCount = rand.rand(7) + 3

taskSet = SabPeon::TaskSet.new
$output = {}
$check = {}

taskCount.times do |i|
	lottonum = rand.rand(10)
	taskSet.inputs[i] = lottonum
	$check[i] = lottonum

	taskSet.task(i) do
		$output[id] = inputs[id]
	end
	
end

taskSet.go!

puts "task count = #{taskCount}"
$check.each do |i,ev|
	av = $output[i]
	puts "#{i} exp=#{ev} act=#{av}"
	taskCount -= 1 if av == ev 
end

if taskCount == 0
	puts "\n\n PASS!"
else
	puts "\n\nFAIL!"
end

